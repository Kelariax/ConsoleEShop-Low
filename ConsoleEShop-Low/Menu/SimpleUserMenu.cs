﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    class SimpleUserMenu : RegisteredUserMenu
    {
        public SimpleUserMenu(DataBase dataBase, Account account)
            : base(dataBase, account)
        {
        }
        public override void OutputUserFunctions() => Console.WriteLine("<<Console E-Shop>><<User>>\n1 - Show all goods\n2 - Search good by name\n3 - Make new order\n" +
            "4 - Confirm order\n5 - Cancel order\n6 - Show your orders\n7 - Set order status received\n8 - Change personal info\n9 - Log out");
        public void ShowOrders()
        {
            int count = 0;
            int page = 1;
            Console.WriteLine($"<<{Account.Login} order list>><<Page: {page}>>");
            foreach (var item in Account.OrdersId)
            {
                Console.WriteLine(DataBase.orderDB.Find(item) + "\n");
                count++;
                if (count % 5 == 0)
                {
                    Console.WriteLine("Any key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        break;
                    }
                    Console.Clear();
                    page++;
                    Console.WriteLine($"<<{Account.Login} order list>><<Page: {page}>>");
                }
            }
            Console.ReadLine();
        }
        public void CancelOrder()
        {
            int id;
            do
            {
                Console.WriteLine("<<Order canceling>>");
                Console.Write("Enter order id: ");

                if (!int.TryParse(Console.ReadLine(), out id))
                {
                    Console.WriteLine("\nWrong input value!");
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {
                    if (Account.ContainsOrder(id))
                    {
                        Order order = DataBase.orderDB.Find(id);
                        Console.Clear();
                        Console.WriteLine("<<Order canceling>>");
                        if (order.Status == OrderStatus.CanceledByAdmin || order.Status == OrderStatus.CanceledByUser)
                        {
                            Console.WriteLine(order);

                            Console.WriteLine("\nOrder already canceled!");
                            Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                            if (!ContinueCheck())
                            {
                                Console.Clear();
                                break;
                            }
                        }
                        else if (order.Status == OrderStatus.Received || order.Status == OrderStatus.Completed)
                        {
                            Console.WriteLine(order);
                            Console.WriteLine("\nOrder already received or completed!");
                            Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                            if (!ContinueCheck())
                            {
                                Console.Clear();
                                break;
                            }
                        }
                        else
                        {
                            order.Status = OrderStatus.CanceledByUser;
                            Console.WriteLine(order);
                            Console.WriteLine("\nOrder successfully canceled!");
                            Console.WriteLine("\nPress any key...");
                            Console.ReadKey();
                            break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("\nYou don't have such order!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                }
                Console.Clear();
            } while (true);
        }

        public void SetOrderRecieved()
        {
            int id;
            do
            {
                Console.WriteLine("<<Order canceling>>");
                Console.Write("Enter order id: ");

                if (!int.TryParse(Console.ReadLine(), out id))
                {
                    Console.WriteLine("\nWrong input value!");
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {
                    if (Account.ContainsOrder(id))
                    {
                        Order order = DataBase.orderDB.Find(id);
                        Console.Clear();
                        Console.WriteLine("<<Order recieving>>");
                        if (order.Status == OrderStatus.CanceledByAdmin || order.Status == OrderStatus.CanceledByUser)
                        {
                            Console.WriteLine(order);

                            Console.WriteLine("\nOrder canceled!");
                            Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                            if (!ContinueCheck())
                            {
                                Console.Clear();
                                break;
                            }
                        }
                        else if (order.Status == OrderStatus.Received || order.Status == OrderStatus.Completed)
                        {
                            Console.WriteLine(order);
                            Console.WriteLine("\nOrder already received or completed!");
                            Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                            if (!ContinueCheck())
                            {
                                Console.Clear();
                                break;
                            }
                        }
                        else if(order.Status != OrderStatus.Sent)
                        {
                            Console.WriteLine(order);
                            Console.WriteLine("\nOrder hasn't sent yet!");
                            Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                            if (!ContinueCheck())
                            {
                                Console.Clear();
                                break;
                            }
                        }
                        else
                        {
                            order.Status = OrderStatus.Received;
                            Console.WriteLine(order);
                            Console.WriteLine("\nOrder status successfully set received!");
                            Console.WriteLine("\nPress any key...");
                            Console.ReadKey();
                            break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("\nYou don't have such order!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                }
                Console.Clear();
            } while (true);
        }
    }
}
