﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    class GuestMenu : UserMenu
    {
        public GuestMenu(DataBase dataBase, Account account)
                : base(dataBase, account)
        {

        }

        public override void OutputUserFunctions() => Console.WriteLine("<<Console E-Shop>><<Guest>>\n1 - Show all goods\n2 - Search good by name\n3 - Sign up\n4 - Log in");
        public Account RegisterUser()
        {
            Account account = null;
            do
            {
                Console.WriteLine("<<User registration>>");
                Console.Write("Enter login: ");
                string login = Console.ReadLine();
                if (login.Length != 0)
                {
                    if (DataBase.accountsDB.Contains(login))
                    {
                        Console.WriteLine("Account with that login already exists!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("You didn't try anything!");
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                Console.Write("Enter password: ");
                string password = Console.ReadLine();
                if (password.Length < 8)
                {
                    Console.WriteLine("\nPassword length must be more than or equal 8");
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {
                    account = new Account(login, password);
                    Console.WriteLine("\nThanks you for registration!");
                    Console.ReadLine();
                    break;
                }
                Console.Clear();
            } while (true);
            Console.Clear();
            return account;
        }
        public Account LogIn()
        {
            Account account = null;
            int index;
            do
            {
                Console.WriteLine("<<User authorization>>");
                Console.Write("Enter login: ");
                string login = Console.ReadLine();
                Console.Write("Enter password: ");
                string password = Console.ReadLine();
                index = DataBase.accountsDB.IndexOf(login);
                if (index < 0 || !DataBase.accountsDB[index].PasswordVerification(password))
                {
                    Console.WriteLine("\nWrong login or password");
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {
                    account = DataBase.accountsDB[index];
                    Console.WriteLine("\nSuccessful authorization!");
                    Console.ReadLine();
                    break;
                }
                Console.Clear();
            } while (true);
            Console.Clear();
            return account;
        }
    }
}
