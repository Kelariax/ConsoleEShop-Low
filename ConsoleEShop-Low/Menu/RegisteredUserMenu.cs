﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    abstract class RegisteredUserMenu : UserMenu
    {
        protected RegisteredUserMenu(DataBase dataBase, Account account)
            : base(dataBase, account)
        {
        }
        public void CreateOrder()
        {
            Order order = null;
            do
            {
                Console.WriteLine("<<Create order>>");
                Console.Write("Enter good name: ");
                string name = Console.ReadLine();
                if (!DataBase.goodsDB.Contains(name))
                {
                    Console.WriteLine("\nThere is no such good!");
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {
                    int quantity;
                    Console.Write("Enter quantity of good: ");
                    if (!int.TryParse(Console.ReadLine(), out quantity))
                    {
                        Console.WriteLine("\nWrong input value!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                    else
                    {
                        if (order == null)
                        {
                            order = Account.CreateNewOrder(DataBase.goodsDB.Find(name), quantity);
                            DataBase.orderDB.Add(order);
                        }
                        else
                        {
                            order.Add(new OrderItem(DataBase.goodsDB.Find(name), quantity));
                        }
                        Console.WriteLine("\nGood was added to the order");

                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            if (order != null)
                            {
                                Console.Clear();
                                Console.WriteLine("<<Create order>>");
                                Console.WriteLine(order);
                                Console.WriteLine("\nOrder successfully created");
                                Console.WriteLine("\nPress any key...");
                                Console.ReadKey();
                            }
                            Console.Clear();
                            break;
                        }
                    }
                }
                Console.Clear();
            } while (true);
        }

        public void ConfirmOrder()
        {
            int id;
            do
            {
                Console.WriteLine("<<Order confirming>>");
                Console.Write("Enter order id: ");

                if (!int.TryParse(Console.ReadLine(), out id))
                {
                    Console.WriteLine("\nWrong input value!");
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {
                    if (Account.ContainsOrder(id))
                    {
                        Order order = DataBase.orderDB.Find(id);

                        Console.Clear();
                        Console.WriteLine("<<Order confirming>>");

                        if (!order.isConfirmed)
                        {
                            if (Account.ConfirmOrder(order))
                            {
                                Console.WriteLine(order);
                                Console.WriteLine("\nOrder successfully confirmed!");
                                Console.WriteLine("\nPress any key...");
                                Console.ReadKey();
                                break;
                            }
                            else
                            {
                                Console.WriteLine(order);
                                Console.WriteLine("\nYou can confirm only 'New' orders!");
                                Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                                if (!ContinueCheck())

                                {
                                    Console.Clear();
                                    break;
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine(order);
                            Console.WriteLine("\nOrder already confirmed!");
                            Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                            if (!ContinueCheck())

                            {
                                Console.Clear();
                                break;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("\nYou don't have such order!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                }
                Console.Clear();
            } while (true);
        }

        public void EditPersonalInfo(Account account)
        {
            int command;
            do
            {
                Console.WriteLine($"<<Editing personal info>><<Account: {account.Login}>>");
                Console.WriteLine(account.personInfo);
                Console.WriteLine("\n1 - Name\n2 - Surname\n3 - Phone\n4 - Email\n5 - Back");
                Console.Write("Choose command: ");
                if (!int.TryParse(Console.ReadLine(), out command))
                {
                    Console.WriteLine("\nWrong input value!");
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else if (command == 3)
                {
                    Console.Clear();
                    Console.WriteLine($"<<Editing personal info>><<Account: {account.Login}>>");
                    Console.WriteLine(account.personInfo);

                    Console.Write("\nEnter new phone (10 numbers):");
                    long phone;

                    if (!long.TryParse(Console.ReadLine(), out phone))
                    {
                        Console.WriteLine("\nWrong input value!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                    else if (phone > PersonInfo.phoneLimit)
                    {

                        account.personInfo.Phone = phone;
                        Console.Clear();
                        Console.WriteLine($"<<Editing personal info>><<Account: {account.Login}>>");
                        Console.WriteLine(account.personInfo);
                        Console.WriteLine($"\nField phone successfully changed!");
                        Console.WriteLine("\nPress any key...");
                        Console.ReadKey();
                    }
                }
                else if (command == 1 || command == 2 || command == 4)
                {
                    Console.Clear();
                    Console.WriteLine($"<<Editing personal info>><<Account: {account.Login}>>");
                    Console.WriteLine(account.personInfo);
                    string commandStr = string.Empty;
                    switch (command)
                    {
                        case 1:
                            commandStr = "name";
                            break;
                        case 2:
                            commandStr = "surname";
                            break;
                        case 4:
                            commandStr = "email";
                            break;
                    }
                    Console.Write($"\nEnter new {commandStr}: ");
                    string value = Console.ReadLine();
                    if (value == string.Empty)
                    {
                        Console.WriteLine("\nWrong string value!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                    else
                    {
                        switch (command)
                        {
                            case 1:
                                account.personInfo.Name = value;
                                break;
                            case 2:
                                account.personInfo.Surname = value;
                                break;
                            case 4:
                                account.personInfo.Email = value;
                                break;
                        }
                        Console.Clear();
                        Console.WriteLine($"<<Editing personal info>><<Account: {account.Login}>>");
                        Console.WriteLine(account.personInfo);
                        Console.WriteLine($"\nField {commandStr} successfully changed!");
                        Console.WriteLine("\nPress any key...");
                    }
                }
                else if (command == 5)
                {
                    break;
                }
                Console.Clear();
            } while (true);
        }

        public void LogOut(out Account account)
        {
            account = null;
        }
    }
}
