﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class PersonInfo
    {
        public const int phoneLimit = 999999999;
        private string _name = "Empty field";
        public string Name
        {
            get { return _name; }
            set
            {
                if (value.Length > 0)
                {
                    _name = value;
                }
            }
        }
        private string _surname = "Empty field";
        public string Surname
        {
            get { return _surname; }
            set
            {
                if (value.Length > 0)
                {
                    _surname = value;
                }
            }
        }
        private long _phone;
        public long Phone 
        {
            get
            {
                return _phone;
            }
            set
            {
                if(value > phoneLimit)
                {
                    _phone = value;
                }
            }   
        }
        private string _email = "Empty field";
        public string Email
        {
            get { return _email; }
            set
            {
                if (value.Length > 0)
                {
                    _email = value;
                }
            }
        }

        public PersonInfo()
        {

        }
        public override string ToString()
        {
            return new string($"Name: {Name}\nSurname: {Surname}\nPhone: {Phone}\nEmail: {Email}");
        }
    }
}
