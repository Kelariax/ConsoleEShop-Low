﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{

    class EShopController
    {
        public DataBase dataBase;

        public EShopController(DataBase dataBase)
        {
            this.dataBase = dataBase;
        }
        public EShopController(IEnumerable<Good> goods, IEnumerable<Account> accounts, IEnumerable<Order> orders)
        {
            this.dataBase = new DataBase(goods, accounts, orders);
        }

        public void Run()
        {
            Account account = null;
            UserMenu userMenu = new GuestMenu(dataBase, account);
         
            int command;
            while (true)
            {
                command = 0;
                try
                {
                    if (account == null)
                    {
                        userMenu = new GuestMenu(dataBase, account);
                        ((GuestMenu)userMenu).OutputUserFunctions();
                        Console.Write("Choose command: ");

                        command = Convert.ToInt32(Console.ReadLine());

                        Console.Clear();
                        switch (command)
                        {
                            case 1:
                                userMenu.OutputGoods();
                                Console.Clear();
                                break;
                            case 2:
                                userMenu.FindGood();
                                break;
                            case 3:
                                account = ((GuestMenu)userMenu).RegisterUser();
                                break;
                            case 4:
                                account = ((GuestMenu)userMenu).LogIn();
                                break;
                            default:
                                throw new ArgumentException(new string("Wrong input data! There is no chosen command!"));
                        }
                    }
                    else
                    {
                        switch (account.Permision)
                        {
                            case PermisionStatus.RegisteredUser:

                                userMenu = new SimpleUserMenu(dataBase, account);
                                ((SimpleUserMenu)userMenu).OutputUserFunctions();
                                Console.Write("Choose command: ");
                                command = Convert.ToInt32(Console.ReadLine());
                                Console.Clear();
                                switch (command)
                                {
                                    case 1:
                                        userMenu.OutputGoods();
                                        Console.Clear();
                                        break;
                                    case 2:
                                        userMenu.FindGood();
                                        break;
                                    case 3:
                                        ((SimpleUserMenu)userMenu).CreateOrder();
                                        break;
                                    case 4:
                                        ((SimpleUserMenu)userMenu).ConfirmOrder();
                                        break;
                                    case 5:
                                        ((SimpleUserMenu)userMenu).CancelOrder();
                                        break;
                                    case 6:
                                        ((SimpleUserMenu)userMenu).ShowOrders();
                                        break;
                                    case 7:
                                        ((SimpleUserMenu)userMenu).SetOrderRecieved();
                                        break;
                                    case 8:
                                        ((SimpleUserMenu)userMenu).EditPersonalInfo(account);
                                        break;
                                    case 9:
                                        ((SimpleUserMenu)userMenu).LogOut(out account);
                                        break;
                                    default:
                                        throw new ArgumentException(new string("Wrong input data! There is no chosen command!"));
                                }
                                break;

                            case PermisionStatus.Admin:
                                userMenu = new AdminUserMenu(dataBase, account);
                                ((AdminUserMenu)userMenu).OutputUserFunctions();
                                Console.Write("Choose command: ");
                                command = Convert.ToInt32(Console.ReadLine());
                                Console.Clear();
                                switch (command)
                                {
                                    case 1:
                                        userMenu.OutputGoods();
                                        Console.Clear();
                                        break;
                                    case 2:
                                        userMenu.FindGood();
                                        break;
                                    case 3:
                                        ((AdminUserMenu)userMenu).CreateOrder();
                                        break;
                                    case 4:
                                        ((AdminUserMenu)userMenu).ConfirmOrder();
                                        break;
                                    case 5:
                                        ((AdminUserMenu)userMenu).ChangeUserInfo();
                                        break;
                                    case 6:
                                        ((AdminUserMenu)userMenu).CreateNewGood();
                                        break;
                                    case 7:
                                        ((AdminUserMenu)userMenu).ChangeGoodInfo();
                                        break;
                                    case 8:
                                        ((AdminUserMenu)userMenu).ChangeOrderStatus();
                                        break;
                                    case 9:
                                        ((AdminUserMenu)userMenu).LogOut(out account);
                                        break;
                                    default:
                                        throw new ArgumentException(new string("Wrong input data! There is no chosen command!"));
                                }
                                break;
                        }
                    }


                }
                catch (Exception ex)
                {
                    Console.Clear();
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!userMenu.ContinueCheck())
                    {
                        break;
                    }
                }
                Console.Clear();
            }

        }
    }
}
