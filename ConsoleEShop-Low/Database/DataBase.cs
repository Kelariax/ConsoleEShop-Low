﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class DataBase
    {
        public GoodsDB goodsDB;
        public AccountDB accountsDB;
        public OrderDB orderDB;

        public DataBase()
        {
            goodsDB = new GoodsDB();
            accountsDB = new AccountDB();
            orderDB = new OrderDB();
        }
        public DataBase(IEnumerable<Good> goods, IEnumerable<Account> accounts, IEnumerable<Order> orders)
        {
            goodsDB = new GoodsDB(goods);
            accountsDB = new AccountDB(accounts);
            orderDB = new OrderDB(orders);
        }

    }
}
