﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleEShop_Low
{
    public class GoodsDB : IEnumerable<Good>, IDatabase<Good, string>
    {
        public List<Good> Goods { get; private set; }
        public GoodsDB()
        {
            Goods = new List<Good>();
        }
        public GoodsDB(Good good)
            : this()
        {
            Goods.Add(good);
        }
        public GoodsDB(IEnumerable<Good> goods)
        {
            Goods = new List<Good>(goods);
        }

        public Good this[int index]
        {
            get
            {
                if(index >= 0 && index < Goods.Count)
                {
                    return Goods[index];
                }

                throw new ArgumentOutOfRangeException(new string("Index is out of range"));
            }
        }

        public void Add(Good item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(new string("The good is null"));
            }
            if(Goods.Contains(item))
            {
                throw new ArgumentException("That item already exists in collection", nameof(item));
            }
            Goods.Add(item);
        }       

        public Good Find(string name)
        {
            int index = IndexOf(name);
            if(index < 0)
            {
                return null;
            }
            return this[index];
        }

        public int IndexOf(string name)
        {
            int count = 0;
            if (this.Contains(name))
            {
                foreach (var item in Goods)
                {
                    if (item.Name == name)
                    {
                        return count;
                    }
                    count++;
                }
            }
            return -1;
        }
        public bool Contains(Good item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(new string("The good is null"));
            }
            foreach (Good good in Goods)
            {
                if (good.Equals(item))
                {
                    return true;
                }
            }
            return false;
        }
        public bool Contains(string name)
        {
            foreach (Good item in Goods)
            {
                if((item.Name).Equals(name))
                {
                    return true;    
                }
            }
            return false;
        }
        public IEnumerator<Good> GetEnumerator()
        {
            return Goods.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Goods.GetEnumerator();
        }
    }
}
