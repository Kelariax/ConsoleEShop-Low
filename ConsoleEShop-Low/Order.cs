﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public enum OrderStatus
    {
        New, CanceledByAdmin, CanceledByUser, Sent, Received, ReceivedPayment, Completed,        
    }
    public class Order
    {
        public int UserId { get; }
        public int Id { get; }

        public DateTime OrderTime{ get; }
        public static int Count { get;  private set; }
        public OrderStatus Status { get; set; }

        public bool isConfirmed;
        public List<OrderItem> orderItems;

        static Order()
        {
            Count = 0;
        }
        public Order(Good good, int userId, int goodQuantity)
        {
            Count++;
            Id = Count;
            UserId = userId;
            orderItems = new List<OrderItem>();
            orderItems.Add(new OrderItem(good, goodQuantity));
            isConfirmed = false;
            Status = OrderStatus.New;
            OrderTime = DateTime.Now;
        }


        public void Add(OrderItem orderItem)
        {
            if (orderItem == null || orderItem.Good == null)
            {
                throw new ArgumentNullException(nameof(orderItem));
            }
            foreach (var item in orderItems)
            {
                if(item.Good.Equals(orderItem.Good))
                {
                    item.GoodQuantity += orderItem.GoodQuantity;
                    return;
                }
            }
            orderItems.Add(orderItem);
        }
        
        public override string ToString()
        {
            string itemsStr = "";
            decimal sum = 0;
            foreach (var item in orderItems)
            {
                itemsStr += item.ToString() + "\n";
                sum += item.Good.Price * item.GoodQuantity;
            }
            return new string($"Order Id: {Id}\nDate: {OrderTime.ToShortDateString()} || Order status: {Status} || Is confirmed: {isConfirmed}\nList of goods:\n{itemsStr}Total price: {Math.Round(sum, 2)}");
        }
    }
}
