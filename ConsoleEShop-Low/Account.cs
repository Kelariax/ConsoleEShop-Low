﻿using System;
using System.Collections.Generic;

namespace ConsoleEShop_Low
{
    public enum PermisionStatus { RegisteredUser, Admin }
    public class Account : IAuthorization
    {
        public PermisionStatus Permision { get; private set; }
        public int Id { get; }
        private static int Count {get; set;}

        public PersonInfo personInfo { get; set; }
        public List<int> OrdersId { get; private set; }

        public string Login { get; }
        public string Password { get; set; }
        
        static Account()
        {
            Count = 0;
        }
        public Account(string login, string password)
        {
            
            Count++;
            Id = Count;
            Login = login;            
            Password = password;
            Permision = PermisionStatus.RegisteredUser;
            OrdersId = new List<int>();
            personInfo = new PersonInfo();
        }
        public Account(string login, string password, PermisionStatus permision)
            :this(login, password)
        {
            Permision = permision;
        }

        public bool PasswordVerification(string password)
        {
            return password == this.Password;
        }
        public Order CreateNewOrder(Good good, int quantity)
        {
            if(good == null)
            {
                throw new ArgumentNullException(new string("The good is null"));
            }
            Order order = new Order(good, this.Id, quantity);
            OrdersId.Add(order.Id); 
            return order;                
        }
        public bool ConfirmOrder(Order order)
        {
            if(order == null)
            {
                throw new ArgumentNullException(nameof(order));
            }
            if(order.Status == OrderStatus.New)
            {
                return order.isConfirmed = true;
            }
            return false;
        }
        public bool ContainsOrder(int id)
        {
            foreach (var item in OrdersId)
            {
                if (id == item)
                {
                    return true;
                }
            }
            return false;
        }
        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return false;
            }
            else if (obj.GetType() != this.GetType())
            {
                return false;
            }
            Account account = (Account)obj;
            if (this.Login == account.Login)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.Login.GetHashCode();
        }

        public override string ToString()
        {
            return new string(Login);
        }
    }
}
