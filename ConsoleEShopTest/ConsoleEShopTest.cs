using System;
using Xunit;
using ConsoleEShop_Low;

namespace ConsoleEShopTest
{
    public class ConsoleEShopTest
    {
        [Fact]

        #region AccountTests
        public void AccountPasswordVerificationTrueValue()
        {
            //Arrange
            var account = new Account("Someone", "Something");

            //Act + Assert
            Assert.True(account.PasswordVerification("Something"));
        }

        [Fact]
        public void AccountPasswordVerificationLowercaseFalseCheck()
        {
            //Arrange
            var account = new Account("Someone", "something");

            //Act + Assert
            Assert.True(!account.PasswordVerification("Something"));
        }
        [Fact]
        public void AccountPasswordVerificationNullFalseCheck()
        {
            //Arrange
            var account = new Account("Someone", "Something");

            //Act + Assert
            Assert.True(!account.PasswordVerification(null));
        }

        [Fact]
        public void ConfirmOrderCheckTrue()
        {
            //Arrange
            var account = new Account("Someone", "Something");
            var order = new Order(new Good { Name = "Shirt", Category = "Clothing", Price = 19.99M }, account.Id, 5);
            //Act + Assert
            Assert.True(account.ConfirmOrder(order));
        }

        [Fact]
        public void ConfirmOrderNullThrowsException()
        {
            //Arrange
            var account = new Account("Someone", "Something");
            //Act + Assert
            Assert.Throws<ArgumentNullException>(() => account.ConfirmOrder(null));
        }

        [Fact]
        public void AccountContainsOrderIdTrue()
        {
            //Arrange

            var account = new Account("Someone", "Something");
            var order = new Order(new Good { Name = "Shirt", Category = "Clothing", Price = 19.99M }, account.Id, 5);
            account.OrdersId.Add(order.Id);
            //Act + Assert
            Assert.True(account.ContainsOrder(order.Id));
        }

        [Fact]
        public void AccountContainsOrderIdFalse()
        {
            //Arrange

            var account = new Account("Someone", "Something");
            var order = new Order(new Good { Name = "Shirt", Category = "Clothing", Price = 19.99M }, account.Id, 5);

            //Act + Assert
            Assert.True(!account.ContainsOrder(order.Id));
        }

        [Fact]
        public void AccountEqualsTrue()
        {
            //Arrange

            var account = new Account("Someone", "Something");
            var account2 = new Account("Someone", "Somewhere");

            //Act + Assert
            Assert.True(account.Equals(account2));
        }
        [Fact]
        public void AccountEqualsFalse()
        {
            //Arrange

            var account = new Account("Someone", "Something");
            var account2 = new Account("Something", "Somewhere");

            //Act + Assert
            Assert.True(!account.Equals(account2));
        }
        [Fact]
        public void AccountEqualsNullFalse()
        {
            //Arrange

            var account = new Account("Someone", "Something");
            Account account2 = null;

            //Act + Assert
            Assert.True(!account.Equals(account2));
        }
        #endregion

        #region OrderTests
        [Fact]
        public void OrderCreationIdCheck()
        {
            //Arrange
            var good = new Good { Name = "Shirt", Category = "Clothing", Price = 19.99M };
            var order = new Order(good, 0, 5);

            //Act + Assert
            Assert.Equal(Order.Count, order.Id);
        }
        [Fact]
        public void OrderAddOrderItemThatAlreadyAdded()
        {
            //Arrange
            var good = new Good { Name = "Shirt", Category = "Clothing", Price = 19.99M };
            var order = new Order(good, 0, 5);
            order.Add(new OrderItem(good, 5));

            //Act + Assert
            Assert.Equal(10, order.orderItems[0].GoodQuantity);
            Assert.Single(order.orderItems);
        }
        [Fact]
        public void OrderAddOrderItemWithNullGood()
        {
            //Arrange
            Good good = null;
            var order = new Order(good, 0, 5);


            //Act + Assert
            Assert.Throws<ArgumentNullException>(() => order.Add(new OrderItem(good, 5)));
            
        }
        [Fact]
        public void OrderAddNullOrderItem()
        {
            //Arrange
            Good good = null;
            var order = new Order(good, 0, 5);


            //Act + Assert
            Assert.Throws<ArgumentNullException>(() => order.Add(null));

        }
        #endregion

        #region AccountDBTests
        [Fact]
        public void AccountDBIndexerReturnsAccount()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            var account = new Account("third", "third");
            accounts.Add(new Account("first", "first"));
            accounts.Add(new Account("second", "second"));
            accounts.Add(account);
            accounts.Add(new Account("fourth", "fourth"));
            accounts.Add(new Account("fifth", "fifth"));

            //Act + Assert

            Assert.Equal(account, accounts[2]);
        }
        [Fact]
        public void AccountDBIndexerOutOfRangeThrowsException()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            var account = new Account("third", "third");
            accounts.Add(new Account("first", "first"));
            accounts.Add(new Account("second", "second"));
            accounts.Add(account);
            accounts.Add(new Account("fourth", "fourth"));
            accounts.Add(new Account("fifth", "fifth"));

            //Act + Assert

            Assert.Throws<ArgumentOutOfRangeException>(() => accounts[7]);
        }
        [Fact]
        public void AccountDBAddNewAccount()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            var account = new Account("third", "third");
            accounts.Add(account);

            //Act + Assert

            Assert.True(accounts.Contains(account));
        }
        [Fact]
        public void AccountDBAddNullAccount()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            Account account = null;

            //Act + Assert

            Assert.Throws<ArgumentNullException>(() => accounts.Add(account));
        }
        [Fact]
        public void AccountDBAddAccountThatAlreadyExist()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            Account account = new Account("third", "third");
            accounts.Add(new Account("third", "third"));

            //Act + Assert

            Assert.Throws<ArgumentException>(() => accounts.Add(account));
        }
        [Fact]
        public void AccountDBFindExistedAccount()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            Account account = new Account("third", "third");
            accounts.Add(new Account("third", "third"));

            //Act + Assert

            Assert.Equal(account, accounts.Find("third"));
        }
        [Fact]
        public void AccountDBIndexOfExistedAccount()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            accounts.Add(new Account("first", "first"));
            accounts.Add(new Account("second", "second"));
            accounts.Add(new Account("third", "third"));
            accounts.Add(new Account("fourth", "fourth"));
            accounts.Add(new Account("fifth", "fifth"));

            //Act + Assert
            Assert.Equal(4, accounts.IndexOf("fifth"));
        }
        [Fact]
        public void AccountDBIndexOfNotExistedAccount()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            accounts.Add(new Account("first", "first"));
            accounts.Add(new Account("second", "second"));
            accounts.Add(new Account("third", "third"));
            accounts.Add(new Account("fourth", "fourth"));
            accounts.Add(new Account("fifth", "fifth"));

            //Act + Assert
            Assert.Equal(-1, accounts.IndexOf("sixth"));
        }
        [Fact]
        public void AccountDBContainsLoginReturnsTrue()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            accounts.Add(new Account("third", "third"));

            //Act + Assert
            Assert.True(accounts.Contains("third"));
        }
        [Fact]
        public void AccountDBContainsLoginReturnsFalse()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            accounts.Add(new Account("third", "third"));

            //Act + Assert
            Assert.True(!accounts.Contains("second"));
        }
        [Fact]
        public void AccountDBContainsAccountReturnsTrue()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            var account = new Account("third", "third");
            accounts.Add(new Account("third", "third"));

            //Act + Assert
            Assert.True(accounts.Contains(account));
        }
        [Fact]
        public void AccountDBContainsAccountReturnsFalse()
        {
            //Arrange
            AccountDB accounts = new AccountDB();
            var account = new Account("second", "second");
            accounts.Add(new Account("third", "third"));

            //Act + Assert
            Assert.True(!accounts.Contains(account));
        }
        #endregion

        #region GoodsDBTests
        [Fact]
        public void GoodsDBIndexerReturnsGood()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            var good = new Good("First", "First", "First", 1.10M);
            goods.Add(good);
            goods.Add(new Good("Second", "Second", "Second", 2.20M));
            goods.Add(new Good("Third", "Third", "Third", 3.30M));
            goods.Add(new Good("Fourth", "Fourth", "Fourth", 4.40M));
            goods.Add(new Good("Fifth", "Fifth", "Fifth", 5.50M));

            //Act + Assert

            Assert.Equal(good, goods[0]);
        }
        [Fact]
        public void GoodsDBIndexerOutOfRangeThrowsException()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            var good = new Good("First", "First", "First", 1.10M);
            goods.Add(good);
            goods.Add(new Good("Second", "Second", "Second", 2.20M));
            goods.Add(new Good("Third", "Third", "Third", 3.30M));
            goods.Add(new Good("Fourth", "Fourth", "Fourth", 4.40M));
            goods.Add(new Good("Fifth", "Fifth", "Fifth", 5.50M));

            //Act + Assert

            Assert.Throws<ArgumentOutOfRangeException>(() => goods[7]);
        }
        [Fact]
        public void GoodsDBAddNewGood()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            var good = new Good("First", "First", "First", 1.10M);
            goods.Add(good);

            //Act + Assert

            Assert.True(goods.Contains(good));
        }
        [Fact]
        public void GoodsDBAddNullGood()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            Good good = null;

            //Act + Assert

            Assert.Throws<ArgumentNullException>(() => goods.Add(good));
        }
        [Fact]
        public void GoodsDBAddGoodThatAlreadyExist()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            var good = new Good("First", "First", "First", 1.10M);
            goods.Add(new Good("First", "First", "First", 1.10M));

            //Act + Assert
            Assert.Throws<ArgumentException>(() => goods.Add(good));
        }
        [Fact]
        public void GoodsDBFindExistedGood()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            var good = new Good("First", "First", "First", 1.10M);
            goods.Add(new Good("First", "First", "First", 1.10M));

            //Act + Assert

            Assert.Equal(good, goods.Find("First"));
        }
        [Fact]
        public void GoodsDBIndexOfExistedGood()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            goods.Add(new Good("First", "First", "First", 1.10M));
            goods.Add(new Good("Second", "Second", "Second", 2.20M));
            goods.Add(new Good("Third", "Third", "Third", 3.30M));
            goods.Add(new Good("Fourth", "Fourth", "Fourth", 4.40M));
            goods.Add(new Good("Fifth", "Fifth", "Fifth", 5.50M));

            //Act + Assert
            Assert.Equal(4, goods.IndexOf("Fifth"));
        }
        [Fact]
        public void OrdersDBIndexOfNotExistedGood()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            goods.Add(new Good("First", "First", "First", 1.10M));
            goods.Add(new Good("Second", "Second", "Second", 2.20M));
            goods.Add(new Good("Third", "Third", "Third", 3.30M));
            goods.Add(new Good("Fourth", "Fourth", "Fourth", 4.40M));
            goods.Add(new Good("Fifth", "Fifth", "Fifth", 5.50M));

            //Act + Assert
            Assert.Equal(-1, goods.IndexOf("Sixth"));
        }
        [Fact]
        public void GoodsDBContainsNameReturnsTrue()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            goods.Add(new Good("First", "First", "First", 1.10M));

            //Act + Assert
            Assert.True(goods.Contains("First"));
        }
        [Fact]
        public void GoodsDBContainsNameReturnsFalse()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            goods.Add(new Good("First", "First", "First", 1.10M));

            //Act + Assert
            Assert.True(!goods.Contains("second"));
        }
        [Fact]
        public void GoodsDBContainsGoodReturnsTrue()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            var good = new Good("First", "First", "First", 1.10M);
            goods.Add(new Good("First", "First", "First", 1.10M));

            //Act + Assert
            Assert.True(goods.Contains(good));
        }
        [Fact]
        public void GoodsDBContainsGoodReturnsFalse()
        {
            //Arrange
            GoodsDB goods = new GoodsDB();
            var good = new Good("Second", "Second", "Second", 1.10M);
            goods.Add(new Good("First", "First", "First", 1.10M));

            //Act + Assert
            Assert.True(!goods.Contains(good));
        }
        #endregion

        #region OrdersDBTests
        [Fact]
        public void OrdersDBIndexerReturnsORder()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            var order = new Order(new Good("First", "First", "First", 1.10M), 1, 1);
            orders.Add(order);
            orders.Add(new Order(new Good("Second", "Second", "Second", 2.20M), 2, 2));
            orders.Add(new Order(new Good("Third", "Third", "Third", 3.30M), 3, 3));
            orders.Add(new Order(new Good("Fourth", "Fourth", "Fourth", 4.40M), 4, 4));
            orders.Add(new Order(new Good("Fifth", "Fifth", "Fifth", 5.50M), 5, 5));

            //Act + Assert

            Assert.Equal(order, orders[0]);
        }
        [Fact]
        public void OrderDBIndexerOutOfRangeThrowsException()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            var order = new Order(new Good("First", "First", "First", 1.10M), 1, 1);
            orders.Add(order);
            orders.Add(new Order(new Good("Second", "Second", "Second", 2.20M), 2, 2));
            orders.Add(new Order(new Good("Third", "Third", "Third", 3.30M), 3, 3));
            orders.Add(new Order(new Good("Fourth", "Fourth", "Fourth", 4.40M), 4, 4));
            orders.Add(new Order(new Good("Fifth", "Fifth", "Fifth", 5.50M), 5, 5));

            //Act + Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => orders[7]);
        }
        [Fact]
        public void OrdersDBAddNewOrder()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            var order = new Order(new Good("First", "First", "First", 1.10M), 1, 1);
            orders.Add(order);

            //Act + Assert

            Assert.True(orders.Contains(order));
        }
        [Fact]
        public void OrdersDBAddNullOrder()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            Order order = null;

            //Act + Assert

            Assert.Throws<ArgumentNullException>(() => orders.Add(order));
        }
        [Fact]
        public void OrdersDBAddOrderThatAlreadyExist()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            var order = new Order(new Good("First", "First", "First", 1.10M), 1, 1);
            orders.Add(order);


            //Act + Assert
            Assert.Throws<ArgumentException>(() => orders.Add(order));
        }
        [Fact]
        public void OrdersDBFindExistedOrder()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            var order = new Order(new Good("First", "First", "First", 1.10M), 1, 1);
            orders.Add(order);

            //Act + Assert

            Assert.Equal(order, orders.Find(Order.Count));
        }
        [Fact]
        public void OrdersDBIndexOfExistedOrder()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            orders.Add(new Order(new Good("First", "First", "First", 1.10M), 1, 1));
            orders.Add(new Order(new Good("Second", "Second", "Second", 2.20M), 2, 2));
            orders.Add(new Order(new Good("Third", "Third", "Third", 3.30M), 3, 3));
            orders.Add(new Order(new Good("Fourth", "Fourth", "Fourth", 4.40M), 4, 4));
            orders.Add(new Order(new Good("Fifth", "Fifth", "Fifth", 5.50M), 5, 5));

            //Act + Assert
            Assert.Equal(4, orders.IndexOf(Order.Count));
        }
        [Fact]
        public void OrdersDBIndexOfNotExistedOrder()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            orders.Add(new Order(new Good("First", "First", "First", 1.10M), 1, 1));
            orders.Add(new Order(new Good("Second", "Second", "Second", 2.20M), 2, 2));
            orders.Add(new Order(new Good("Third", "Third", "Third", 3.30M), 3, 3));
            orders.Add(new Order(new Good("Fourth", "Fourth", "Fourth", 4.40M), 4, 4));
            orders.Add(new Order(new Good("Fifth", "Fifth", "Fifth", 5.50M), 5, 5));

            //Act + Assert
            Assert.Equal(-1, orders.IndexOf(Order.Count+1));
        }
        [Fact]
        public void OrderDBContainsIDReturnsTrue()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            orders.Add(new Order(new Good("First", "First", "First", 1.10M), 1, 1));
           
            //Act + Assert
            Assert.True(orders.Contains(Order.Count));
        }
        [Fact]
        public void OrdersDBContainsIDReturnsFalse()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            orders.Add(new Order(new Good("First", "First", "First", 1.10M), 1, 1));

            //Act + Assert
            Assert.True(!orders.Contains(2));
        }
        [Fact]
        public void OrdersDBContainsOrderReturnsTrue()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            var order = new Order(new Good("Second", "Second", "Second", 2.20M), 2, 2);
            orders.Add(order);

            //Act + Assert
            Assert.True(orders.Contains(order));
        }
        [Fact]
        public void OrdersDBContainsOrderReturnsFalse()
        {
            //Arrange
            OrderDB orders = new OrderDB();
            var order = new Order(new Good("Second", "Second", "Second", 2.20M), 2, 2);
            orders.Add(new Order(new Good("First", "First", "First", 1.10M), 1, 1));

            //Act + Assert
            Assert.True(!orders.Contains(order));
        }
        #endregion
    }
}
